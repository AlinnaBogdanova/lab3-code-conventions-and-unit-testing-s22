package com.hw.db.controllers;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;

import javax.websocket.server.PathParam;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


class threadControllerTests {
//     private User loggedIn;
//     private Forum toCreate;
    private Timestamp timestamp;
    private Thread createdThread;
    private String userName = "alina";
    private int threadId = 10;
    private String threadSlug = "Sl1Ug";
    threadController controller;
    private User user;

    @BeforeEach
    @DisplayName("Thread creation")
    void createThreadTest() {
//         loggedIn = new User("some","some@email.mu", "name", "nothing");
//         toCreate = new Forum(12, "some", 3, "title", "some");
        timestamp = Timestamp.valueOf("2022-02-22 12:00:00");
        threadId = 10;
        createdThread = new Thread(userName, timestamp, "TestForum", "HelloWorld", threadSlug, "Title", 2);
        createdThread.setId(threadId);
        controller = new threadController();
        user = new User(userName, "a.bogdanova@innopolis.university", userName, "");
    }

    @Test
    @DisplayName("Gets thread by ID")
    void checkGetThreadById() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(createdThread);
            assertEquals(createdThread, controller.CheckIdOrSlug(Integer.toString(threadId)));
        }
    }

    @Test
    @DisplayName("Gets thread by Slug")
    void checkGetThreadBySlug() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(createdThread);
            assertEquals(createdThread, controller.CheckIdOrSlug(threadSlug));
        }
    }

    @Test
    @DisplayName("Post creation")
    void checkCreatePost() {
        // creation of the inputs to the function
        Post post = new Post(userName, timestamp, "forum", "message", 0, threadId, false);
        List<Post> list = new ArrayList<Post>();
        list.add(post);

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            // due to the previous validation of the checkIdOrSlug
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(createdThread);

            try(MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                userDAOMock.when(() -> UserDAO.Info(userName)).thenReturn(user);

                // checks path with no catch statements
                assertEquals(controller.createPost(threadSlug, list), ResponseEntity.status(HttpStatus.CREATED).body(list));
            }
        }
    }

    @Test
    @DisplayName("Post listing")
    void checkListPost() {
        // creation of the inputs to the function
        Post post = new Post(userName, timestamp, "forum", "message", 0, threadId, false);
        List<Post> answerList = new ArrayList<Post>();
        answerList.add(post);

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getPosts(threadId, 10, 0, "", false)).thenReturn(answerList);
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(createdThread);
            assertEquals(controller.Posts(threadSlug, 10, 0, "", false), ResponseEntity.status(HttpStatus.OK).body(answerList));
        }
    }

    @Test
    @DisplayName("Thread change")
    void checkThreadChange() {
        // yes, this test is stupid
        Thread newThread = new Thread(userName, timestamp, "NewTestForum", "HelloWorld", threadSlug, "Title", 5);

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(createdThread);
            threadDAOMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(newThread);

            assertEquals(controller.change(threadSlug, newThread), ResponseEntity.status(HttpStatus.OK).body(newThread));
        }
    }

    @Test
    @DisplayName("Gets info of the thread")
    void checkInfo() {
        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(createdThread);
            assertEquals(controller.info(threadSlug), ResponseEntity.status(HttpStatus.OK).body(createdThread));
        }
    }

    @Test
    @DisplayName("Upvote")
    void checkVote() {

        Vote vote = new Vote(userName, 2);

        try (MockedStatic<ThreadDAO> threadDAOMock = Mockito.mockStatic(ThreadDAO.class)) {
            // due to the previous validation of the checkIdOrSlug
            threadDAOMock.when(() -> ThreadDAO.getThreadBySlug(threadSlug)).thenReturn(createdThread);

            try(MockedStatic<UserDAO> userDAOMock = Mockito.mockStatic(UserDAO.class)) {
                userDAOMock.when(() -> UserDAO.Info(userName)).thenReturn(user);

                // checks path with no catch statements
                assertNotEquals(controller.createVote(threadSlug, vote), ResponseEntity.status(HttpStatus.CREATED).body(createdThread));
            }
        }
    }

}

